let userName = "";

while (userName == "") {
    userName = prompt("Як вас звати?");
}

let userAge = NaN;
let userAgeString = "";
while (isNaN(userAge) || userAge == 0) {
    userAgeString = prompt("Скільки вам років?", userAgeString);
    userAge = Number(userAgeString);
}

if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge <= 22) {
    let confirmResult = confirm("Are you sure you want to continue? ");
    if (confirmResult === false) {
        alert("You are not allowed to visit this website");
    } else if (confirmResult === true) {
        alert("Welcome, " + userName);
    }
} else if (userAge > 22) {
    alert("Welcome, " + userName);
}

 
